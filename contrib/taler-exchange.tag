<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile doxygen_version="1.9.4">
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path>/research/taler/exchange/src/include/</path>
    <filename>d5/dcb/taler__error__codes_8h.html</filename>
    <member kind="enumeration">
      <type></type>
      <name>TALER_ErrorCode</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_ACCOUNT_UNKNOWN</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aaeffc5c26407ed73d7638a5588de7c1dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_BAD_IF_NONE_MATCH</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aab4c0f2469a013643db71e9d1093a17f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_BAD_IF_MATCH</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aa5deccc6c9d196e9a5d15d2b9e8a462ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_BAD_SYNC_SIGNATURE</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aa6c88e2f7b362aa75422e188c205501d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_INVALID_SIGNATURE</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aa0c79eda43c2de90e28521b466bfeb958</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_MALFORMED_CONTENT_LENGTH</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aaa811843c4362e3b91ff16563a7f11a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_EXCESSIVE_CONTENT_LENGTH</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aac67bcfb7de5cd35d554280caaec494f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_OUT_OF_MEMORY_ON_CONTENT_LENGTH</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aa8dced986853a382e1fe1478cd4115d57</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_INVALID_UPLOAD</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aa4ad086f449ddbabebd4c09af46054728</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_PAYMENT_GENERIC_TIMEOUT</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aacdf25dccb3b6e11b8705f1954e881aac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_PAYMENT_CREATE_BACKEND_ERROR</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aabbb8e55ff86dbe38c25747fadb64e2a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_PREVIOUS_BACKUP_UNKNOWN</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aaeb094aed793a576fdf76fdbca7643686</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_MISSING_CONTENT_LENGTH</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aad04abf794980705173d77fd28435d08a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_GENERIC_BACKEND_ERROR</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aab534c9117dccea53d74e87ace8e4fea1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TALER_EC_SYNC_GENERIC_BACKEND_TIMEOUT</name>
      <anchorfile>d5/dcb/taler__error__codes_8h.html</anchorfile>
      <anchor>a05ff23648cb502e6128a53a5aef8d49aa264068a45ffad5779c0de179fdf79658</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_mhd_lib.h</name>
    <path>/research/taler/exchange/src/include/</path>
    <filename>df/d6d/taler__mhd__lib_8h.html</filename>
    <includes id="d5/dcb/taler__error__codes_8h" name="taler_error_codes.h" local="yes" imported="no">taler_error_codes.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>TALER_SIGNATURE_SYNC_BACKUP_UPLOAD</name>
      <anchorfile>dc/d61/taler__signatures_8h.html</anchorfile>
      <anchor>af15ac86c81f8c3993b56a59a4ccdaa1a</anchor>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>
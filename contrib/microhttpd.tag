<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>microhttpd_lib.h</name>
    <path></path>
    <filename>microhttpd.h</filename>
    <member kind="define">
      <type>#define</type>
      <name>MHD_YES</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_NO</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_OK</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_NOT_FOUND</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_CONFLICT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_NO_CONTENT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_GET</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_PUT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_DELETE</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_POST</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_VERSION_1_1</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_OPTION_NOTIFY_COMPLETED</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>function</type>
      <name>MHD_run</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>function</type>
      <name>MHD_get_connection_values</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>MHD_AccessHandlerCallback</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>MHD_RequestCompletedCallback</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>

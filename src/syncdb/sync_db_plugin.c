/*
  This file is part of TALER
  Copyright (C) 2019 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file syncdb/sync_db_plugin.c
 * @brief Logic to load database plugin
 * @author Christian Grothoff
 * @author Sree Harsha Totakura <sreeharsha@totakura.in>
 */
#include "platform.h"
#include "sync_util.h"
#include "sync_database_lib.h"
#include <ltdl.h>


struct SYNC_DatabasePlugin *
SYNC_DB_plugin_load (const struct GNUNET_CONFIGURATION_Handle *cfg,
                     bool skip_preflight)
{
  char *plugin_name;
  char *lib_name;
  struct SYNC_DatabasePlugin *plugin;

  if (GNUNET_SYSERR ==
      GNUNET_CONFIGURATION_get_value_string (cfg,
                                             "sync",
                                             "db",
                                             &plugin_name))
  {
    GNUNET_log_config_missing (GNUNET_ERROR_TYPE_ERROR,
                               "sync",
                               "db");
    return NULL;
  }
  (void) GNUNET_asprintf (&lib_name,
                          "libsync_plugin_db_%s",
                          plugin_name);
  GNUNET_free (plugin_name);
  plugin = GNUNET_PLUGIN_load (SYNC_project_data (),
                               lib_name,
                               (void *) cfg);
  if (NULL == plugin)
  {
    GNUNET_free (lib_name);
    return NULL;
  }
  plugin->library_name = lib_name;
  if ( (! skip_preflight) &&
       (GNUNET_OK !=
        plugin->preflight (plugin->cls)) )
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Database not ready. Try running sync-dbinit!\n");
    SYNC_DB_plugin_unload (plugin);
    return NULL;
  }
  return plugin;
}


void
SYNC_DB_plugin_unload (struct SYNC_DatabasePlugin *plugin)
{
  char *lib_name;

  if (NULL == plugin)
    return;
  lib_name = plugin->library_name;
  GNUNET_assert (NULL == GNUNET_PLUGIN_unload (lib_name,
                                               plugin));
  GNUNET_free (lib_name);
}


/* end of sync_db_plugin.c */

/*
  This file is part of Sync
  Copyright (C) 2020,2024 Taler Systems SA

  Sync is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Sync is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Sync; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file sync/sync-httpd_config.c
 * @brief headers for /config handler
 * @author Christian Grothoff
 */
#include "platform.h"
#include "sync-httpd_config.h"
#include <taler/taler_json_lib.h>


/*
 * Protocol version history:
 *
 * 0: original design
 * 1: adds ?fresh=y to POST backup operation to force fresh contract
 *    to be created
 */

MHD_RESULT
SH_handler_config (struct SH_RequestHandler *rh,
                   struct MHD_Connection *connection,
                   void **connection_cls,
                   const char *upload_data,
                   size_t *upload_data_size)
{
  static struct MHD_Response *response;
  static struct GNUNET_TIME_Absolute a;

  (void) connection_cls;
  (void) upload_data;
  (void) upload_data_size;
  if ( (GNUNET_TIME_absolute_is_past (a)) &&
       (NULL != response) )
  {
    MHD_destroy_response (response);
    response = NULL;
  }
  if (NULL == response)
  {
    struct GNUNET_TIME_Timestamp km;
    char dat[128];

    a = GNUNET_TIME_relative_to_absolute (GNUNET_TIME_UNIT_DAYS);
    /* Round up to next full day to ensure the expiration
       time does not become a fingerprint! */
    a = GNUNET_TIME_absolute_round_down (a,
                                         GNUNET_TIME_UNIT_DAYS);
    a = GNUNET_TIME_absolute_add (a,
                                  GNUNET_TIME_UNIT_DAYS);
    /* => /config response stays at most 48h in caches! */
    km = GNUNET_TIME_absolute_to_timestamp (a);
    TALER_MHD_get_date_string (km.abs_time,
                               dat);
    response = TALER_MHD_MAKE_JSON_PACK (
      GNUNET_JSON_pack_string ("name",
                               "sync"),
      GNUNET_JSON_pack_string ("implementation",
                               "urn:net:taler:specs:sync:c-reference"),
      GNUNET_JSON_pack_uint64 ("storage_limit_in_megabytes",
                               SH_upload_limit_mb),
      TALER_JSON_pack_amount ("liability_limit",
                              &SH_insurance),
      TALER_JSON_pack_amount ("annual_fee",
                              &SH_annual_fee),
      GNUNET_JSON_pack_string ("version",
                               "2:2:0"));
    GNUNET_break (MHD_YES ==
                  MHD_add_response_header (response,
                                           MHD_HTTP_HEADER_EXPIRES,
                                           dat));
    GNUNET_break (MHD_YES ==
                  MHD_add_response_header (response,
                                           MHD_HTTP_HEADER_CACHE_CONTROL,
                                           "public,max-age=21600")); /* 6h */
  }
  return MHD_queue_response (connection,
                             MHD_HTTP_OK,
                             response);
}


/* end of sync-httpd_config.c */

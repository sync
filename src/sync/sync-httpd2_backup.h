/*
  This file is part of TALER
  Copyright (C) 2014--2025 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file sync-httpd2_backup.h
 * @brief functions to handle incoming requests on /backup/
 * @author Christian Grothoff
 */
#ifndef SYNC_HTTPD2_BACKUP_H
#define SYNC_HTTPD2_BACKUP_H

#include <microhttpd2.h>

/**
 * Service is shutting down, resume all MHD requests NOW.
 */
void
SH_resume_all_bc (void);


/**
 * Return the current backup of @a account on @a request
 * using @a default_http_status on success.
 *
 * @param account account to query
 * @param default_http_status HTTP status to queue response
 *  with on success (#MHD_HTTP_STATUS_OK or #MHD_HTTP_STATUS_CONFLICT)
 * @return MHD response
 */
struct MHD_Response *
SH_make_backup (const struct SYNC_AccountPublicKeyP *account,
                enum MHD_HTTP_StatusCode default_http_status);


/**
 * Handle request on @a request for retrieval of the latest
 * backup of @a account.
 *
 * @param request the MHD request to handle
 * @param account public key of the account the request is for
 * @return MHD action
 */
const struct MHD_Action *
SH_backup_get (struct MHD_Request *request,
               const struct SYNC_AccountPublicKeyP *account);


/**
 * Handle POST /backup requests.
 *
 * @param request the MHD request to handle
 * @param account public key of the account the request is for
 * @param upload_size number of bytes being uploaded
 * @return MHD result code
 */
const struct MHD_Action *
SH_backup_post (struct MHD_Request *request,
                const struct SYNC_AccountPublicKeyP *account,
                uint_fast64_t upload_size);


#endif

/*
  This file is part of Sync
  Copyright (C) 2020 Taler Systems SA

  Sync is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Sync is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Sync; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file sync/sync-httpd_config.h
 * @brief headers for /config handler
 * @author Christian Grothoff
 */
#ifndef SYNC_HTTPD2_CONFIG_H
#define SYNC_HTTPD2_CONFIG_H
#include <microhttpd2.h>
#include "sync-httpd2.h"

/**
 * Manages a /config call.
 *
 * @param request the MHD request to handle
 * @param upload_size number of bytes that were uploaded
 * @return MHD action
 */
const struct MHD_Action *
SH_handler_config (struct MHD_Request *request,
                   uint_fast64_t upload_size);

#endif

/* end of sync-httpd_config.h */

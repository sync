/*
  This file is part of TALER
  Copyright (C) 2019-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file sync/sync-httpd2.h
 * @brief HTTP serving layer
 * @author Christian Grothoff
 */
#ifndef SYNC_HTTPD2_H
#define SYNC_HTTPD2_H

#include "platform.h"
#include <microhttpd2.h>
#include <taler/taler_mhd2_lib.h>
#include "sync_database_lib.h"

/**
 * @brief Struct describing an URL and the handler for it.
 */
struct SH_RequestHandler
{

  /**
   * URL the handler is for. Must not start with "/backups/".
   */
  const char *url;

  /**
   * HTTP Method the handler is for.
   */
  enum MHD_HTTP_Method method;

  /**
   * Function to call to handle the request.
   *
   * @param request the MHD request to handle
   * @param upload_size number of bytes to be uploaded
   * @return MHD action
   */
  const struct MHD_Action *
  (*handler)(struct MHD_Request *request,
             size_t upload_size);
};


/**
 * Each MHD response handler that sets the "connection_cls" to a
 * non-NULL value must use a struct that has this struct as its first
 * member.  This struct contains a single callback, which will be
 * invoked to clean up the memory when the contection is completed.
 */
struct TM_HandlerContext;

/**
 * Signature of a function used to clean up the context
 * we keep in the "connection_cls" of MHD when handling
 * a request.
 *
 * @param hc header of the context to clean up.
 */
typedef void
(*TM_ContextCleanup)(struct TM_HandlerContext *hc);


/**
 * Each MHD response handler that sets the "connection_cls" to a
 * non-NULL value must use a struct that has this struct as its first
 * member.  This struct contains a single callback, which will be
 * invoked to clean up the memory when the connection is completed.
 */
struct TM_HandlerContext
{

  /**
   * Function to execute the handler-specific cleanup of the
   * (typically larger) context.
   */
  TM_ContextCleanup cc;

  /**
   * Asynchronous request context id.
   */
  struct GNUNET_AsyncScopeId async_scope_id;
};


/**
 * Handle to the database backend.
 */
extern struct SYNC_DatabasePlugin *db;

/**
 * Upload limit to the service, in megabytes.
 */
extern unsigned long long SH_upload_limit_mb;

/**
 * Annual fee for the backup account.
 */
extern struct TALER_Amount SH_annual_fee;

/**
 * Our Taler backend to process payments.
 */
extern char *SH_backend_url;

/**
 * Our fulfillment URL
 */
extern char *SH_fulfillment_url;

/**
 * Our context for making HTTP requests.
 */
extern struct GNUNET_CURL_Context *SH_ctx;

/**
 * Amount of insurance.
 */
extern struct TALER_Amount SH_insurance;

/**
 * Kick MHD to run now, to be called after MHD_resume_connection().
 * Basically, we need to explicitly resume MHD's event loop whenever
 * we made progress serving a request.  This function re-schedules
 * the task processing MHD's activities to run immediately.
 */
void
SH_trigger_daemon (void);


/**
 * Kick GNUnet Curl scheduler to begin curl interactions.
 */
void
SH_trigger_curl (void);


#endif

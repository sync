/*
  This file is part of TALER
  (C) 2018, 2019 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file include/sync_testing_lib.h
 * @brief API for writing an interpreter to test SYNC components
 * @author Christian Grothoff <christian@grothoff.org>
 */
#ifndef SYNC_TESTING_LIB_H
#define SYNC_TESTING_LIB_H

#include "sync_service.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_testing_lib.h>
#include <microhttpd.h>


/**
 * Make the "backup download" command for a non-existent upload.
 *
 * @param label command label
 * @param sync_url base URL of the sync serving
 *        the policy store request.
 * @return the command
 */
struct TALER_TESTING_Command
SYNC_TESTING_cmd_backup_nx (const char *label,
                            const char *sync_url);


/**
 * Make the "backup download" command.
 *
 * @param label command label
 * @param sync_url base URL of the sync serving
 *        the policy store request.
 * @param http_status expected HTTP status.
 * @param upload_ref reference to upload command
 * @return the command
 */
struct TALER_TESTING_Command
SYNC_TESTING_cmd_backup_download (const char *label,
                                  const char *sync_url,
                                  unsigned int http_status,
                                  const char *upload_ref);


/**
 * Types of options for performing the upload. Used as a bitmask.
 */
enum SYNC_TESTING_UploadOption
{
  /**
   * Do everything by the book.
   */
  SYNC_TESTING_UO_NONE = 0,

  /**
   * Use random hash for previous upload instead of correct
   * previous hash.
   */
  SYNC_TESTING_UO_PREV_HASH_WRONG = 1,

  /**
   * Request payment.
   */
  SYNC_TESTING_UO_REQUEST_PAYMENT = 2,

  /**
   * Reference payment order ID from linked previous upload.
   */
  SYNC_TESTING_UO_REFERENCE_ORDER_ID = 4


};


/**
 * Make the "backup upload" command.
 *
 * @param label command label
 * @param sync_url base URL of the sync serving
 *        the policy store request.
 * @param prev_upload reference to a previous upload we are
 *        supposed to update, NULL for none
 * @param last_upload reference to the last upload for the
 *          same account, used to check result on MHD_HTTP_CONFLICT
 * @param uo upload options
 * @param http_status expected HTTP status.
 * @param backup_data data to upload
 * @param backup_data_size number of bytes in @a backup_data
 * @return the command
 */
struct TALER_TESTING_Command
SYNC_TESTING_cmd_backup_upload (const char *label,
                                const char *sync_url,
                                const char *prev_upload,
                                const char *last_upload,
                                enum SYNC_TESTING_UploadOption uo,
                                unsigned int http_status,
                                const void *backup_data,
                                size_t backup_data_size);

/**
 * Call @a op on all simple traits.
 *
 * @param op macro to call
 */
#define SYNC_TESTING_SIMPLE_TRAITS(op) \
        op (sync_account_pub, const struct SYNC_AccountPublicKeyP)   \
        op (sync_account_priv, const struct SYNC_AccountPrivateKeyP) \
        op (prev_hash, const struct GNUNET_HashCode) \
        op (curr_hash, const struct GNUNET_HashCode)


/* FIXME: eventually switch to libgnunettesting with the SYNC_ prefix for
   the symbols instead of TALER_TESTING_! */
SYNC_TESTING_SIMPLE_TRAITS (TALER_TESTING_MAKE_DECL_SIMPLE_TRAIT)

#endif
